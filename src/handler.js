const { nanoid } = require('nanoid');
const notes = require('./notes');
const HapiUrl = require('hapi-url')

const homeStart = (request, h) => {
    const url = HapiUrl;

    const response = h.response({
        status: 'Success',
        message: `Server Started at : ${url.current(request)}`
    });
    response.code(200);
    return response;
}

const addNoteHandler = (request, h) => {
    const { title, tags, body } = request.payload;
    const id = nanoid(16);
    const createdAt = new Date().toISOString();
    const updatedAt = createdAt;

    const newNote = {
        title, tags, body, id, createdAt, updatedAt
    }

    notes.push(newNote);

    const isSuccess = notes.filter((note) => note.id === id).length > 0;

    if (isSuccess) {
        const response = h.response({
            status: 'success',
            message: 'data berhasil ditambahkan',
            data: {
                noteId: id,
            },
        });

        response.code(201);
        return response;
    };

    const response = h.response({
        status: 'Failed',
        message: 'data Gagal ditambahkan'
    });

    response.code(500);
    return response;
};

const getAllNotesHandler = () => ({
    status: 'success',
    data: {
        notes,
    },
});

const getNoteByIdHandler = (request, h) => {
    const { id } = request.params;

    const note = notes.filter((note) => note.id === id)[0];

    if (note !== undefined) {
        return {
            status: 'success',
            data: {
                note,
            },
        };
    };

    const response = h.response({
        status: 'fail',
        message: 'catatan tidak ditemukan',
    })

    response.code(404);
    return response
};

const editNoteByIdHandler = (request, h) => {
    const { id } = request.params
    const { title, tags, body } = request.payload
    const updatedAt = new Date().toISOString();

    const index = notes.findIndex((note) => note.id === id);

    if (index !== -1) {
        notes[index] = {
            ...notes[index],
            title,
            tags,
            body,
            updatedAt
        };

        const response = h.response({
            status: 'success',
            message: 'catatan berhasil diperbarui'
        });
        response.code(200)
        return response
    };

    const response = h.response({
        status: 'failed',
        message: 'catatan gagal diperbarui, ID tidak ditemukan',
    });

    response.code(404);
    return response

};

const deleteNoteByIdHandler = (request, h) => {
    const { id } = request.params

    const index = notes.findIndex((note) => note.id === id);

    if (index !== -1) {
        notes.splice(index, 1);
        const response = h.response({
            status: 'success',
            message: 'catatan berhasil dihapus'
        });

        response.code(200);
        return response
    }

    const response = h.response({
        status: 'success',
        message: 'catatan Gagal diperbarui. ID tidak ditemukan'
    });

    response.code(404);
    return response;
}


module.exports = {
    homeStart,
    addNoteHandler,
    getAllNotesHandler,
    getNoteByIdHandler,
    editNoteByIdHandler,
    deleteNoteByIdHandler,
};
